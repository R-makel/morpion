import { FormEvent, useRef } from 'react';
//import { NonNullableUserNames } from '../../lib/tictactoe/helpers';
import './usernamesform.css'

type NomJoueur = {
  X: string | null;
  O: string | null;
};

//Création d'un type UserNames sans la possibilité d'avoir une valeur null donc pas la peine de créer une opération 
type NonNullableUserNames = DeepNonNullable<NomJoueur>;

type DeepNonNullable<T> = {
  [P in keyof T]: NonNullable<T[P]>;
};

type NomJoueurFormProps = {
  nomJoueurSubmitted: (nomJoueur: NonNullableUserNames) => void;
};

export const NomJoueurForm = ({ nomJoueurSubmitted }: NomJoueurFormProps) => {

  //on ajoute le type générique "HTMLInputElement" afin que les ref savent vers quel type d'element il pointe
  const joueurXRef = useRef<HTMLInputElement>(null);
  const joueurORef = useRef<HTMLInputElement>(null);

const onSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const joueurX = joueurXRef.current?.value;
    const joueurO = joueurORef.current?.value;
    //Si il manque un nom on ne peut pas atteindre le jeux
    if (!joueurX || !joueurO) {
      return;
    }
    //Si les playeurs ont le même nom ont retourne une arrete
    if (joueurX === joueurO) {
      alert('Usernames must be different');
      return;
    }
    nomJoueurSubmitted({ X: joueurX, O: joueurO });
  };

  return (
    <form onClick={onSubmit} className="vertical-stack">
      <h3>Renseignez vos noms</h3>
      <label htmlFor="user1">Joueur X</label>
      <input id="user1" ref={joueurXRef} required minLength={2} />
      <label htmlFor="user2">Joueur O</label>
      <input id="user2" ref={joueurORef} required minLength={2} />
      <button type="submit">Submit</button>
    </form>
  );
};
