import { useState, createContext, PropsWithChildren,useContext } from 'react';

import {
    calculJoueurSuivant,
    calculateStatus,
    getDefaultSquares,
    NonNullableUserNames,
    ValeurCarre ,
    NomJoueurs,
    calculWinner
} from '../../lib/tictactoe/helpers';

//Le type qu'on va attribuer aux arguements du context  
type GameContextOutput = {
    carres: ValeurCarre [];
    xNomJoueur: string | null;
    oNomJoueur: string | null;
    status: string;
    carresGagnant: number[];
    setNomJoueur: (userNames: NomJoueurs) => void;
    onSquareClick: (index:number) => void;
};
export const GameContext = createContext<GameContextOutput | null>(null) ;

export const JeuProvider = ({children}:PropsWithChildren) => {
    // les cases ont pour valeur 'X' ,ou 'O' ou "null" grace au type "SquarValue[]"
    const [carres, setCarres] = useState<ValeurCarre[]>(() => getDefaultSquares());
  
    const [nomJoueur, setNomJoueur] = useState<NomJoueurs>({
      X: null,
      O: null,
    });
    
    const nextValue = calculJoueurSuivant(carres);
  
    const onSquareClick = (index:number) =>{
  
      if(carres[index] || index < 0 || index > carres.length - 1){
        return;
      }
      setCarres(curr => {
        const newSquares = [...curr];
        newSquares[index] = nextValue;
        return newSquares ;
      })
    }
  
  
    const xNomJoueur = nomJoueur.X;
    const oNomJoueur = nomJoueur.O;
  
    const {gagnant, carresGagnant} = calculWinner(carres) ;
  
    const status = calculateStatus(
      carres,
      `${nomJoueur[nextValue]}'s turn (${nextValue})`,
      gagnant ? nomJoueur[gagnant] : gagnant,
    );
  
    const value: GameContextOutput = {
      carres,
      xNomJoueur,
      oNomJoueur,
      status,
      carresGagnant,
      setNomJoueur,
      onSquareClick
    };
  
    return <GameContext.Provider value={value}>{children}</GameContext.Provider>
  
  };