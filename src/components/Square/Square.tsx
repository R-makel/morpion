import clsx from 'clsx';
import { ComponentPropsWithoutRef } from 'react';
import './square.css'

// toutes les props qu'un composant 'button' peut récupérer
type CaseProps = ComponentPropsWithoutRef<'button'> & {
  CaseGagant?: boolean;
};

export const Case = ({ children, CaseGagant, ...props }: CaseProps) => {
  return (
    <button
      //clsx permet de donner un nobre illimité d'argument de n'importe quelle type
      className={clsx('square', {
        'winning-square': CaseGagant,
      })}
      //spread les props à l'intérieur des bouton ce qui permet de faire fonctionner notre Square si on ajoute des évènements dans ses apppels de composant
      {...props}
    >
      {children}
    </button>
  );
};