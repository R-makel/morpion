/* eslint-disable react-hooks/rules-of-hooks */
import { ValeurCarre } from '../../lib/tictactoe/helpers';
import { Case } from '../Square/Square';
import './board.css'

type SurfaceJeuProps = {
  cases: ValeurCarre[];
  carresGagnant?: number[];
  onClick?: (index: number) => void;
};

export const SurfaceJeu = ({ cases, onClick, carresGagnant }: SurfaceJeuProps) => {
  return (
    <div className="game-board">
      {cases.map((square, index) => (
        <Case
          onClick={() => onClick?.(index)}
          //Un tableau d'index qui indique les carré gagnants
          //si le tableau d'index contient l'index de notre élément courant c'est alors un gagnant
          CaseGagant={carresGagnant?.includes(index)}
          key={index}
        >
          {square}
        </Case>
      ))}
    </div>
  );
};