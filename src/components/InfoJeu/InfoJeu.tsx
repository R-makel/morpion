import { NonNullableUserNames } from '../../lib/tictactoe/helpers';

type GameInfoProps = {
  status: string;
  nomsJoueurs: NonNullableUserNames;
};

export const InfoJeu = ({ status, nomsJoueurs }: GameInfoProps) => {
  return (
    <div className= "game-info">
      <div className="flex gap-3 center">
        <span>
          <b>X</b>:{nomsJoueurs.X}
        </span>
        <span>VS</span>
        <span>
          <b>O</b>:{nomsJoueurs.O}
        </span>
      </div>
      <div>{status}</div>
    </div>
  );
};
