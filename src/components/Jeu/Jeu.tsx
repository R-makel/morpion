import { useContext } from 'react';
import { SurfaceJeu } from '../Board/SurfaceJe';
import { InfoJeu } from '../InfoJeu/InfoJeu';
import { NomJoueurForm } from '../UserNamesForm/NomJoueurForm';
import { JeuProvider, GameContext  } from '../JeuProvider/JeuProvider';

// Il doit prendre en paramètre un children
// Il doit retourner le contexte créé plus haut avec le children
  

//refractor du composant 'Jeu' afin de limiter la quantité de code métier et de hook dans 'Game'
const useJeu = () =>{
  
    //ici nous utilisons le contexte permettant de récuperer les donnees sans passé par des props
    const context = useContext(GameContext);
  
    if(context===null){
      throw new Error('useJeu doit utiliser un context');
    }
    return context;
  }

 export const Jeu = () => {
    const { carres, xNomJoueur, oNomJoueur, status, setNomJoueur, onSquareClick, carresGagnant} = useJeu();
  
    //Si on a pas de nom pour UsernameX et/ou UsernameO on retourne le formulaire avant de pouvoir jouer
    if (!xNomJoueur || !oNomJoueur) {
      return (
        <NomJoueurForm
            nomJoueurSubmitted={(nomJoueur) => {
              setNomJoueur(nomJoueur);
              }}
        />
      );
    }
  
    return (
      <div className="game">
        <InfoJeu
          //le status nous dit a qui le tour est
          status={status}
          nomsJoueurs={{
            X: xNomJoueur,
            O: oNomJoueur,
          }}
        />
        <SurfaceJeu onClick = {onSquareClick} carresGagnant={carresGagnant} cases={carres} />
      </div>
    );
  };

  export default function App() {
    return (
      
      <div>
        <h2>TicTacToe</h2>
        <JeuProvider>
         <Jeu/>
        </JeuProvider>
      </div>
    );
    }