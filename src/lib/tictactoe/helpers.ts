export type ValeurCarre = 'X' | 'O' | null;

//regarde comb
export const calculJoueurSuivant = (carres: ValeurCarre []) => {
  //regarde combien il y a de X dans le tableau 
  const xCount = carres.filter((r) => r === 'X').length;
  //regarde combien il y a de O dans le tableau 
  const oCount = carres.filter((r) => r === 'O').length;
  //si il y a autant de X que de O on retourn X sinon on retourne O
  return oCount === xCount ? 'X' : 'O';
};

export const calculateStatus = (
  carres: ValeurCarre [],
  nextPlayer: string,
  winner?: string | null
) => {
  return winner
     //Si il y a un gagnant son nom sera marqué
    ? `Winner: ${winner}`
    : carres.every(Boolean)
    ? `Scratch: Cat's game`
    : `Next player: ${nextPlayer}`;
};

export const getDefaultSquares = (): ValeurCarre [] => new Array(9).fill(null);

export type NomJoueurs = {
  X: string | null;
  O: string | null;
};

export type NonNullableUserNames = DeepNonNullable<NomJoueurs>;

export type DeepNonNullable<T> = {
  [P in keyof T]: NonNullable<T[P]>;
};

export const calculWinner = (carre: ValeurCarre []) => {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (const line of lines) {
    const [a, b, c] = line;
    if (carre[a] && carre[a] === carre[b] && carre[a] === carre[c]) {
      return {
        gagnant: carre[a],
        carresGagnant: line,
      };
    }
  }
  return {
    winner: null,
    carresGagnant: [],
  };
};
